﻿using _7_Wonders.Services;
using _7_Wonders.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using _7_Wonders.Models.DbModels;


namespace _7_Wonders
{
    class MenuViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;

        private IWindowService _windowService;
        private ApplicationContext _dataBase;

        public RelayCommand StartCommand { get; set; }
        public RelayCommand CloseWindowCommand { get; set; }
        public RelayCommand GoCommand { get; set; }
        public RelayCommand LogInCommand { get; set; }
        public RelayCommand RegisterCommand { get; set; }
        public RelayCommand NextPageCommand { get; set; }
        public RelayCommand PreviousPageCommand { get; set; }

        public RelayCommand NextRuleCommand {  get; set; }
        public RelayCommand PreviousRuleCommand {  get; set; }

        private string _help;
        public string Help
        {
            get => _help;
            set
            {
                _help = value;
                OnPropertyChanged(nameof(Help));
            }
        }
        private string _firstPlayerName;
        public string FirstPlayerName
        {
            get => _firstPlayerName;
            set
            {
                _firstPlayerName = value;
                OnPropertyChanged(nameof(FirstPlayerName));
            }
        }

        private string _secondPlayerName;
        public string SecondPlayerName
        {
            get => _secondPlayerName;
            set
            {
                _secondPlayerName = value;
                OnPropertyChanged(nameof(SecondPlayerName));
            }
        }

        private string _login = "";
        public string Login
        {
            get => _login;
            set
            {
                _login = value;
                OnPropertyChanged(nameof(Login));
            }
        }

        private string _password = "";
        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                OnPropertyChanged(nameof(Password));
            }
        }

        private int _pageNum;
        private int _ruleNum;

        public bool[] Pages { get; set; }
        public bool[] RuleVisibility { get; set; }
        private bool _isMuted = true;
        public bool IsMuted
        {
            get => _isMuted;
            set
            {
                _isMuted = value;
                OnPropertyChanged(nameof(IsMuted));
            }
        }
        public List<GameResults> GameResultsList { get; set; }

        public bool[] PageVisibility { get; set; }

        public MenuViewModel(IWindowService windowService)
        {
            FirstPlayerName = "Player1";
            SecondPlayerName = "Player2";
            _pageNum = 0;
            Pages = [true, false, false];
            _ruleNum = 0;
            RuleVisibility = [true, false, false, false];
            PageVisibility =[false, false, false, false, false, false, false, false, false ];
            PageVisibility[Mediator.PageId] = true;
            IsMuted = Mediator.IsMuted;
            Mediator.PageId = 0;
            Login = Mediator.Login;
            GameResultsList = new();

            _windowService = windowService;
            _dataBase = new ApplicationContext();

            if (!string.IsNullOrEmpty(Login))
            {
                GameResultsList = _dataBase.GameResults.Where(u => u.UserId == Mediator.UserId).ToList();
            }
            LogInCommand = new RelayCommand(obj => LogInCommandExecuted(), obj => LogAndRegisterCommandCanExecute());
            RegisterCommand = new RelayCommand(obj => RegisterCommandExecuted(), obj => LogAndRegisterCommandCanExecute());
            StartCommand = new RelayCommand(obj => StartCommandExecuted(), obj => StartCommandCanExecute());
            CloseWindowCommand = new RelayCommand(obj => _windowService.CloseWindow());
            GoCommand = new RelayCommand(obj =>
            {
                int idx = int.Parse((string)obj);
                GoCommandExecuted(idx);
            });
            NextPageCommand = new RelayCommand(obj =>
            {
                Pages[_pageNum] = false;
                _pageNum++;
                if (_pageNum >= Pages.Length)
                {
                    _pageNum = 0;
                }
                Pages[_pageNum] = true;
                OnPropertyChanged(nameof(Pages));
            });
            PreviousPageCommand = new RelayCommand(obj =>
            {
                Pages[_pageNum] = false;
                _pageNum--;
                if (_pageNum < 0)
                {
                    _pageNum = Pages.Length - 1;
                }
                Pages[_pageNum] = true;
                OnPropertyChanged(nameof(Pages));
            });
            NextRuleCommand = new RelayCommand(obj =>
            {
                RuleVisibility[_ruleNum] = false;
                _ruleNum++;
                if (_ruleNum >= RuleVisibility.Length)
                {
                    _ruleNum = 0;
                }
                RuleVisibility[_ruleNum] = true;
                OnPropertyChanged(nameof(RuleVisibility));
            });
            PreviousRuleCommand = new RelayCommand(obj =>
            {
                RuleVisibility[_ruleNum] = false;
                _ruleNum--;
                if (_ruleNum < 0)
                {
                    _ruleNum = RuleVisibility.Length - 1;
                }
                RuleVisibility[_ruleNum] = true;
                OnPropertyChanged(nameof(RuleVisibility));
            });
        }

        public void LogInCommandExecuted()
        {
            User user = _dataBase.Users.FirstOrDefault(u => u.Login == Login.Trim() && u.Password == Password.Trim());
            if (user == null)
            {
                Help = "Не знайдено користувача з таким логіном і паролем";
            }
            else
            {
                PageVisibility[0] = false;
                PageVisibility[2] = true;
                Help = "";
                Mediator.UserId = user.Id;
                Mediator.IsMuted = user.IsSoundMuted;
                IsMuted = user.IsSoundMuted;
                GameResultsList = _dataBase.GameResults.Where(u => u.UserId == Mediator.UserId).ToList();
                OnPropertyChanged(nameof(GameResultsList));
                OnPropertyChanged(nameof(PageVisibility));
            }
        }

        public bool LogAndRegisterCommandCanExecute()
        {
            return Login.Trim() != string.Empty && Password.Trim() != string.Empty && Login.Trim().Length < 11 && Password.Trim().Length < 16;
        }

        public void RegisterCommandExecuted()
        {
            if (_dataBase.Users.FirstOrDefault(u => u.Login == Login.Trim()) != null)
            {
                Help = "Користувач з таким логіном вже існує";
            }
            else
            {
                _dataBase.Users.Add(new User { Login = Login.Trim(), Password = Password.Trim(), IsSoundMuted = false });
                _dataBase.SaveChanges();
                PageVisibility[1] = false;
                PageVisibility[2] = true;
                Help = "";
                GameResultsList = new();
                IsMuted = false;
                Mediator.IsMuted = false;
                Mediator.UserId = _dataBase.Users.FirstOrDefault(u => u.Login == Login.Trim()).Id;
                OnPropertyChanged(nameof(GameResultsList));
                OnPropertyChanged(nameof(PageVisibility));
            }
        }

        public void StartCommandExecuted()
        {
            Mediator.FirstPlayerName = FirstPlayerName.Trim();
            Mediator.SecondPlayerName = SecondPlayerName.Trim();
            Mediator.Login = Login.Trim();

            _windowService.OpenWindow();
        }

        public bool StartCommandCanExecute()
        {
            return FirstPlayerName.Trim() != string.Empty && SecondPlayerName.Trim() != string.Empty && FirstPlayerName != SecondPlayerName
                && FirstPlayerName.Trim().Length <= 10 && SecondPlayerName.Trim().Length <= 10;
        }

        public void GoCommandExecuted(int idx)
        {
            for (int i = 0; i < PageVisibility.Length; i++)
            {
                PageVisibility[i] = false;
            }
            PageVisibility[idx] = true;
            Help = "";
            if (idx == 0 || idx == 1)
            {
                Login = "";
                Password = "";
                IsMuted = true;
                Mediator.Login = Login;
                GameResultsList.Clear();
            }
            else if (idx == 2)
            {
                FirstPlayerName = "Player1";
                SecondPlayerName = "Player2";
                Pages[_pageNum] = false;
                RuleVisibility[_ruleNum] = false;
                _ruleNum = 0;
                _pageNum = 0;
                Pages[_pageNum] = true;
                RuleVisibility[_ruleNum] = true;
                OnPropertyChanged(nameof(Pages));
                OnPropertyChanged(nameof(RuleVisibility));
            }
            if (IsMuted != Mediator.IsMuted && idx != 0 && idx != 1)
            {
                Mediator.IsMuted = IsMuted;
                User user = _dataBase.Users.FirstOrDefault(u => u.Id == Mediator.UserId);
                user.IsSoundMuted = IsMuted;
                _dataBase.SaveChanges();
            }
            OnPropertyChanged(nameof(PageVisibility));
        }
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
