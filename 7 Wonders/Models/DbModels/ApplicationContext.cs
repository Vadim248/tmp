﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7_Wonders.Models.DbModels
{
    public class ApplicationContext : DbContext
    {
        private const string _connectionString = "Data Source=SevenWonder.db";

        public DbSet<User> Users => Set<User>();
        public DbSet<GameResults> GameResults => Set<GameResults>();

        public ApplicationContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(_connectionString);
        }
    }
}
